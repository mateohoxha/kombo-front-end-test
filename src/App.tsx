import { HashRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header/Header";
import styles from "./index.module.scss";
import Home from "./pages/Home";
import TicketInfo from "./pages/TicketInfo";
import TicketsProvider from "./contexts/Tickets/TicketsProvider";

function App() {
  return (
    <HashRouter>
      <TicketsProvider>
        <Header />
        <div className={styles["kombo-content"]}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/ticket" element={<TicketInfo />} />
          </Routes>
        </div>
      </TicketsProvider>
    </HashRouter>
  );
}

export default App;
