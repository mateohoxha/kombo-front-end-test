import React, { useState } from "react";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import Modal from 'react-modal';
import calendarLogo from './../../assets/svg/calendar.png';
import location from './../../assets/svg/location.png';
import plus from './../../assets/svg/plus.png';
import arrows from './../../assets/svg/sort-arrows-couple-pointing-up-and-down.png';
import arrow from './../../assets/svg/up-down.png';
import user from './../../assets/svg/user.png';
import styles from "./SearchForm.module.scss";

Modal.setAppElement('#root');

const modalStyles = {
    overlay: {
        background: 'linear-gradient(90deg,rgba(3,88,175,.6),rgba(32,140,251,.4))'
    },
    content: {
        height: '500px',
        width: '300px',
        borderRadius: 10,
        borderWidth: 0,
        backgroundColor: '#ffffff',
        left: '50%',
        transform: 'translateX(-50%)'
    }
};

const modalStylesCalendar = {
    content: {
        width: '70%',
        height: 'fit-content',
        borderRadius: 10,
        borderWidth: 0,
        backgroundColor: '#ffffff',
        left: '50%',
        transform: 'translateX(-50%)'
    },
};

function SearchForm() {
    const [modalDepartureIsOpen, setIsOpen] = React.useState(false);
    const [modalArrivalIsOpen, setArrivalOpen] = React.useState(false);
    const [calendarIsOpen, setCalendarOpen] = React.useState(false);
    const [calendarArrivalIsOpen, setCalendarArrivalOpen] = React.useState(false);
    const [passengerIsOpen, setPassengerOpen] = React.useState(false);
    const [value, onChange] = useState(new Date());

    function openPassenger() {
        setPassengerOpen(true);
    }

    return (
        <>
            <h1 className={styles['text']}>Look For a Trip</h1>
            <div className={styles['main-container']}>
                <div className={styles["select"]} onClick={() => setIsOpen(true)}>
                    <div className={styles["location"]}>
                        <img src={location} alt="Logo" className={styles['location-logo']} />
                        <h4 className={styles['text']}>City of departure</h4>
                    </div>
                    <img src={arrow} alt="Logo" className={styles['location-logo']} />
                </div>
                <Modal
                    isOpen={modalDepartureIsOpen}
                    onRequestClose={() => setIsOpen(false)}
                    style={modalStyles}
                >
                    <h4 className={styles['text']}>Where do you come from?</h4>
                    <form>
                        <label>
                            <input type="text" name="name" placeholder='City of departure' className={styles['inputBox']} />
                        </label>
                        <h4 className={styles['text']}>Recent</h4>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Paris</h4>
                            <h4 className={styles['locationText']}>France</h4>
                        </div>
                        <h4 className={styles['text']}>Popular</h4>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Paris</h4>
                            <h4 className={styles['locationText']}>France</h4>
                        </div>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>London</h4>
                            <h4 className={styles['locationText']}>UK</h4>
                        </div>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Brussels</h4>
                            <h4 className={styles['locationText']}>Belgium</h4>
                        </div>
                    </form>
                </Modal>
                <div className={styles["select"]} onClick={() => setArrivalOpen(true)}>
                    <div className={styles["location"]}>
                        <img src={location} alt="Logo" className={styles['location-logo']} />
                        <h4 className={styles['text']}>City of arrival</h4>
                    </div>
                    <img src={arrow} alt="Logo" className={styles['location-logo']} />
                </div>
                <Modal
                    isOpen={modalArrivalIsOpen}
                    onRequestClose={() => setArrivalOpen(false)}
                    style={modalStyles}
                >
                    <h4 className={styles['text']}>Where do you want to go?</h4>
                    <form>
                        <label>
                            <input type="text" name="name" placeholder='City of arrival' className={styles['inputBox']} />
                        </label>
                        <h4 className={styles['text']}>Recent</h4>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Paris</h4>
                            <h4 className={styles['locationText']}>France</h4>
                        </div>
                        <h4 className={styles['text']}>Popular</h4>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Paris</h4>
                            <h4 className={styles['locationText']}>France</h4>
                        </div>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>London</h4>
                            <h4 className={styles['locationText']}>UK</h4>
                        </div>
                        <div className={styles['city']}>
                            <h4 className={styles['locationText']}>Brussels</h4>
                            <h4 className={styles['locationText']}>Belgium</h4>
                        </div>
                    </form>
                </Modal>
                <div className={styles["calendar-container"]}>
                    <div className={styles["calendar-left"]} onClick={() => setCalendarOpen(true)}>
                        <div className={styles["location"]}>
                            <img src={calendarLogo} alt="Logo" className={styles['location-logo']} />
                            <h4 className={styles['text']}>01/04/2022</h4>
                        </div>
                    </div>
                    <div className={styles["calendar-right"]} onClick={() => setCalendarArrivalOpen(true)}>
                        <div className={styles["location"]}>
                            <img src={calendarLogo} alt="Logo" className={styles['location-logo']} />
                            <h4 className={styles['text']}>01/04/2022</h4>
                        </div>
                    </div>
                </div>
                <Modal
                    isOpen={calendarIsOpen}
                    onRequestClose={() => setCalendarOpen(false)}
                    style={{
                        overlay: modalStyles.overlay,
                        ...modalStylesCalendar
                    }}
                >
                    <div className={styles['calendarModal']}>
                        <h4 className={styles['text']}>When do you want to leave?</h4>
                    </div>
                    <Calendar onChange={onChange} value={value} className={styles['calendarFull']} />
                </Modal>
                <Modal
                    isOpen={calendarArrivalIsOpen}
                    onRequestClose={() => setCalendarArrivalOpen(false)}
                    style={{
                        overlay: modalStyles.overlay,
                        ...modalStylesCalendar
                    }}
                >
                    <div className={styles['calendarModal']}>
                        <h4 className={styles['text']}>When do you want to come back?</h4>
                        <Calendar onChange={onChange} value={value} className={styles['calendarFull']} />
                    </div>
                </Modal>
                <div className={`${styles["select"]} ${styles['lastSelect']}`} onClick={openPassenger}>
                    <div className={styles["location"]}>
                        <img src={user} alt="Logo" className={styles['location-logo']} />
                        <h4 className={styles['text']}>Passengers</h4>
                    </div>
                    <img src={arrows} alt="Logo" className={styles['location-logo']} onClick={openPassenger} />
                </div>
                <Modal
                    isOpen={passengerIsOpen}
                    onRequestClose={() => setPassengerOpen(false)}
                    style={modalStyles}
                >
                    <div className={styles['calendarModal']}>
                        <h4 className={styles['text']} onClick={openPassenger}>Passengers</h4>
                        <div className={styles["passengerContainer"]}>
                            <img src={user} alt="Logo" className={styles['location-logo']} />
                            <h4 className={styles['text']} onClick={openPassenger}>Adult</h4>
                            <h4 className={styles['age']} onClick={openPassenger}>(12+ years old)</h4>
                            <img src={plus} alt="Logo" className={styles['plus-logo']} />
                        </div>
                        <div className={styles["passengerContainer"]}>
                            <img src={user} alt="Logo" className={styles['location-logo']} />
                            <h4 className={styles['text']} onClick={openPassenger}>Child</h4>
                            <h4 className={styles['age']} onClick={openPassenger}>(4-11 years old)</h4>
                            <img src={plus} alt="Logo" className={styles['plus-logo']} />
                        </div>
                        <div className={styles["passengerContainer"]}>
                            <img src={user} alt="Logo" className={styles['location-logo']} />
                            <h4 className={styles['text']} onClick={openPassenger}>Infant</h4>
                            <h4 className={styles['age']} onClick={openPassenger}>(0-3 years old)</h4>
                            <img src={plus} alt="Logo" className={styles['plus-logo']} />
                        </div>
                        <button className={styles['passengerContainer']}>Continue</button>
                    </div>
                </Modal>
                <button>Search</button>
            </div>
        </>
    );
}
export default SearchForm;
