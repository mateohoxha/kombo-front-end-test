import { parseTime } from "../../../helpers";
import { Segment } from "../../../types";
import { CompanyLogo } from "../../CompanyLogo";
import { TransportIcon } from "../../TransportIcon";
import styles from "./TicketInfoItem.module.scss";

type Props = {
  segment: Segment;
}

function TicketInfoItem({ segment }: Props) {
  return (
    <div className={styles["trip"]}>
      <div className={styles["tripSegment"]}>
        <p className={styles["time"]}>{parseTime(segment.departure.time)}</p>
        <div className={styles["location"]}>
          <p className={styles["city"]}>{segment.departure.city}</p>
          <p className={styles["station"]}>{segment.departure.station}</p>
        </div>
      </div>
      <div className={styles["transport-type"]}>
        <TransportIcon transportType={segment.transportType} />
        <div className={styles["company-logo"]}>
          <CompanyLogo companySlug={segment.companySlug} />
        </div>
      </div>
      <div className={styles["tripSegment"]}>
        <p className={styles["time"]}>{parseTime(segment.arrival.time)}</p>
        <div className={styles["location"]}>
          <p className={styles["city"]}>{segment.arrival.city}</p>
          <p className={styles["station"]}>{segment.arrival.station}</p>
        </div>
      </div>
    </div>
  );
}

export default TicketInfoItem;