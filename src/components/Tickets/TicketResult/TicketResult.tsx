import { Col, Container, Row, setConfiguration } from 'react-grid-system';
import { Link } from "react-router-dom";
import { withTickets } from '../../../contexts/Tickets/withTickets';
import { parseTime } from '../../../helpers';
import { Ticket } from "../../../types";
import { CompanyLogo } from "../../CompanyLogo";
import { TransportIcon } from "../../TransportIcon";
import styles from "./TicketResult.module.scss";

setConfiguration({ maxScreenClass: 'xl' });

type Props = {
  ticket: Ticket;
  ticketsContext: {
    selectTicket: (ticket: Ticket) => {}
  }
}

function TicketResult({ ticket, ticketsContext }: Props) {
  if (!ticket) return null;

  const companySlugs = ticket.segments.map((segment) => segment.companySlug);

  const firstTrip = ticket.segments[0];
  const lastTrip = ticket.segments[ticket.segments.length - 1];
  const price = Math.round(ticket.price) + " €";
  const departureTime = parseTime(firstTrip.departure.time);
  const arrivalTime = parseTime(lastTrip.arrival.time);
  const departureStation = firstTrip.departure.station;
  const arrivalStation = lastTrip.arrival.station;

  return (
    <Link to="/ticket" className={styles['link']} onClick={() => ticketsContext.selectTicket(ticket)}>
      <div className={styles["ticket"]}>
        <div className={styles['header']}>
          <TransportIcon transportType={firstTrip.transportType} />
          {companySlugs.map((slug, i) => (
            <div key={i} className={styles["company-logo"]}>
              <CompanyLogo companySlug={slug} />
            </div>
          ))}
        </div>

        <Container className={styles['info-container']}>
          <Row className={styles['row']}>
            <Col xs={3} style={{ textAlign: 'start', color: '#132968', paddingLeft: 0 }}>
              <h5 className={styles['time']}>{departureTime}</h5>
              <h5 className={styles['time']}>{arrivalTime}</h5>
            </Col>
            <Col xs={5} className={styles['city-main-container']}>
              <div className={styles['station']}>
                <h5 className={styles['station-text']}>{departureStation}</h5>
                <h5 className={styles['station-text']}>{arrivalStation}</h5>
              </div>
            </Col>
            <Col style={{ textAlign: 'end', color: '#208cfb', paddingRight: 0 }} xs={3}>
              <h2>{price}</h2>
            </Col>
          </Row>
        </Container>
      </div>
    </Link>
  );
}

export default withTickets(TicketResult);