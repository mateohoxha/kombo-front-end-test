import React from 'react';
import { Ticket } from '../../types';

export const TicketsContext = React.createContext({
    ticketsContext: {
      allTickets: [] as Ticket[],
      ticket: {} as Ticket,
      selectTicket: (_ticket: Ticket) => {}
    }
});
