import { useState, useEffect, ReactNode } from 'react';
import { TicketsContext } from "./TicketsContext";
import { Ticket, Segment } from '../../types';
import { Api } from '../../service/api';

type Props = {
  children: ReactNode
}

const TicketsProvider = ({ children }: Props) => {
  const [allTickets, setAllTickets] = useState<Ticket[]>([]);
  const [ticket, setTicket] = useState<Ticket>({ price: 0, segments: [] as Segment[] });

  useEffect(() => {
    getAllTickets();
  }, []);

  const getAllTickets = async () => {
    const tickets = await Api.getTickets();
    setAllTickets(tickets);
  };

  const selectTicket = (ticket: Ticket) => {
    setTicket(ticket);
  };

  return (
    <TicketsContext.Provider
      value={{
        ticketsContext: {
          allTickets,
          ticket,
          selectTicket
        }
      }}
    >
      {children}
    </TicketsContext.Provider>
  );
}

export default TicketsProvider;