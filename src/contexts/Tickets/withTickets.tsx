import { TicketsContext } from "./TicketsContext";

export function withTickets(Component: any) {
  return function TicketsComponent(props: any) {
    return (
      <TicketsContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </TicketsContext.Consumer>
    );
  };
}
