export const parseTime = (date: string) => {
    const time = new Date(date).toLocaleTimeString("en-US", {
        hour: "2-digit",
        minute: "2-digit",
    });
    return time;
};

export const formatDate = (date: string) => {
    const options: any = { weekday: 'short', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString("en-US", options)
};