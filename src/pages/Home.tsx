import { Col, Container, Row } from 'react-grid-system';
import { ReactComponent as BusIcon } from "../assets/svg/transportTypes/bus.svg";
import { ReactComponent as PlaneIcon } from "../assets/svg/transportTypes/flight.svg";
import { ReactComponent as TrainIcon } from "../assets/svg/transportTypes/train.svg";
import SearchForm from "../components/SearchForm/SearchForm";
import TicketResult from "../components/Tickets/TicketResult/TicketResult";
import style from "../components/Tickets/TicketResult/TicketResult.module.scss";
import { withTickets } from '../contexts/Tickets/withTickets';
import { Ticket } from '../types';
import styles from "./Home.module.scss";

type Props = {
  ticketsContext: {
    allTickets: Ticket[]
  }
}

function Home(props: Props) {
  return (
    <div className={styles["kombo-search"]}>
      <div className={styles["search-container"]}>
        <SearchForm />
      </div>
      <div className={styles["tickets-container"]}>
        <div>
          <Container className={style['main-container']}>
            <Row className={style['row']}>
              <Col sm={3} className={style['date']}>
                <div>Tue 5 Apr</div>
              </Col>
              <Col sm={4} className={style['active-date']}>
                <div>Wed 6 Apr</div>
              </Col>
              <Col sm={3} className={style['date']}>
                <div>Thu 7 Apr</div>
              </Col>
            </Row>
          </Container>
        </div>
        <div className={style['transport-container']}>
          <Container>
            <Row className={style['row']}>
              <Col xs={3}>
                <div>
                  <TrainIcon />
                  <h5 className={styles['text']}>107$</h5>
                  <h5 className={styles['text']}>3:41</h5>
                </div>
              </Col>
              <Col xs={3}>
                <div>
                  <BusIcon />
                  <h5 className={styles['text']}>31$</h5>
                  <h5 className={styles['text']}>8:25</h5>
                </div>
              </Col>
              <Col xs={3}>
                <div>
                  <PlaneIcon />
                  <h5 className={styles['text-disabled']}>-</h5>
                  <h5 className={styles['text-disabled']}>No results</h5>
                </div>
              </Col>
              <Col xs={3}>
                <div>
                  <PlaneIcon />
                  <h5 className={styles['text']}>166$</h5>
                  <h5 className={styles['text']}>5:00</h5>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        {props.ticketsContext.allTickets.length > 0 ? (
          props.ticketsContext.allTickets.map((ticket, index) => (
            <TicketResult
              key={index}
              ticket={ticket}
            />
          ))
        ) : (
          <p>Loading...</p>
        )}
      </div>
    </div>
  );
}

export default withTickets(Home);