import TicketInfoItem from '../components/Tickets/TicketInfo/TicketInfoItem';
import { withTickets } from "../contexts/Tickets/withTickets";
import { formatDate } from "../helpers";
import { Segment, Ticket } from "../types";
import electrical from './../assets/svg/electricity.png';
import toilet from './../assets/svg/toilet.png';
import wifi from './../assets/svg/wifi.png';
import styles from "./TicketInfo.module.scss";

type Props = {
  ticketsContext: {
    ticket: Ticket;
  }
}

function TicketInfo(props: Props) {
  const { ticketsContext: { ticket } } = props;

  return (
    <div className={styles["ticket-info"]}>
      <div className={styles['outward']}>
        <h3>Outward Journey</h3>
      </div>
      <div className={styles["ticket"]}>
        <div className={styles["date"]}>{formatDate(ticket.segments[0].arrival.time)}</div>
        {ticket.segments.map((segment: Segment, index: number) => (
          <TicketInfoItem key={index} segment={segment} />
        ))}
        <div className={styles["price"]}>Total Price: {ticket.price + " €"}</div>
      </div>
      <div className={styles["ticket"]}>
        <div className={styles["date"]}>Flexibility</div>
        <div className={styles["trip"]}>
          <div className={styles["flexibility"]}>
            <p className={styles["standard"]}>Standard</p>
            <p className={styles["text"]}>Refundable in 1 click until 15 minutes before departure. Flixbus fees to be expected.</p>
          </div>
        </div>
      </div>
      <div className={styles["ticket"]}>
        <div className={styles["date"]}>Included</div>
        <div className={styles["trip"]}>
          <div className={styles["flexibility"]}>
            <p className={styles["standard"]}>Per person: 1 luggage on hand + 1 luggage checked in</p>
            <div className={styles['included']}>
              <div className={styles['amenities']}>
                <img src={wifi}></img><p className={styles['amenities-text']}>Wi-fi</p>
              </div>
              <div className={styles['amenities']}>
                <img src={electrical}></img><p className={styles['amenities-text']}>Electrical outlet</p>
              </div>
              <div className={styles['amenities']}>
                <img src={toilet}></img><p className={styles['amenities-text']}>Toilets</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button className={styles['button']}>Select this ticket {ticket.price + " €"}</button>
    </div>
  );
}

export default withTickets(TicketInfo);